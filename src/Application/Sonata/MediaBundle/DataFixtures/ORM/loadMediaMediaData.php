<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 22/05/13
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Sonata\MediaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Sonata\MediaBundle\Entity\Media;

class loadMediaMediaData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity1 = new Media();
        $entity1->setName('SvnBuild.xml');
        $entity1->setEnabled(true);
        $entity1->setProviderName('sonata.media.provider.file');
        $entity1->setProviderStatus(1);
        $entity1->setProviderReference('svnbuildupload.xml');
        $entity1->setProviderMetadata(array('filename'=>'SvnBuild.xml'));
        $entity1->setContentType('application/xml');
        $entity1->setContext('default');

        $entity2 = new Media();
        $entity2->setName('Koala.jpg');
        $entity2->setProviderName('sonata.media.provider.image');
        $entity2->setProviderStatus(1);
        $entity2->setProviderReference('koalaupload.jpeg');
        $entity2->setProviderMetadata(array('filename' => 'Koala.jpg'));
        $entity2->setWidth(1024);
        $entity2->setHeight(768);
        $entity2->setContentType('image/jpeg');
        $entity2->setContext('default');

        $entity3 = new Media();
        $entity3->setName('Vanessa Paradis & M : "La Seine" (Live)');
        $entity3->setProviderName('sonata.media.provider.youtube');
        $entity3->setProviderStatus(1);
        $entity3->setProviderReference('AkFsoXUa_zs');
        $entity3->setProviderMetadata(array(
            'height' => 270,
            'author_name' => 'gugenspitzer',
            "thumbnail_url" => "http:\/\/i2.ytimg.com\/vi\/AkFsoXUa_zs\/hqdefault.jpg",
            "html" => "<iframe width=\"480\" height=\"270\" src=\"http:\/\/www.youtube.com\/embed\/AkFsoXUa_zs?feature=oembed\" frameborder=\"0\" allowfullscreen><\/iframe>",
            "type" => "video",
            "thumbnail_width" => 480,
            "provider_url" => "http:\/\/www.youtube.com\/",
            "version" => "1.0",
            "thumbnail_height" => 360,
            "author_url" => "http:\/\/www.youtube.com\/user\/gugenspitzer",
            "title" => "Vanessa Paradis & M : \"La Seine\" (Live)",
            "width" => 480,
            "provider_name" => "YouTube"
        ));
        $entity3->setWidth(480);
        $entity3->setHeight(270);
        $entity3->setContentType('video/x-flv');
        $entity3->setAuthorName('gugenspitzer');
        $entity3->setContext('default');

        $manager->persist($entity1);
        $manager->persist($entity2);
        $manager->persist($entity3);
        $manager->flush();

        if (!file_exists('web/uploads/media/default/0001/01'))
            mkdir('web/uploads/media/default/0001/01');

        //Manage pictures entity1
        copy('web/img/svnbuildupload.xml', 'web/uploads/media/default/0001/01/svnbuildupload.xml');

        //Manage pictures entity2
        copy('web/img/koalaupload.jpeg', 'web/uploads/media/default/0001/01/koalaupload.jpeg');
        copy('web/img/koala_admin.jpeg', 'web/uploads/media/default/0001/01/thumb_' . $entity2->getId(). '_admin.jpeg');
        copy('web/img/koala_default_big.jpeg', 'web/uploads/media/default/0001/01/thumb_' . $entity2->getId() . '_default_big.jpeg');
        copy('web/img/koala_default_small.jpeg', 'web/uploads/media/default/0001/01/thumb_' . $entity2->getId() . '_default_small.jpeg');

        //Manage pictures entity3
        copy('web/img/video_admin.jpg', 'web/uploads/media/default/0001/01/thumb_' . $entity3->getId(). '_admin.jpg');
        copy('web/img/video_default_big.jpg', 'web/uploads/media/default/0001/01/thumb_' . $entity3->getId(). '_default_big.jpg');
        copy('web/img/video_default_small.jpg', 'web/uploads/media/default/0001/01/thumb_' . $entity3->getId(). '_default_small.jpg');
        copy('web/img/video_reference.jpg', 'web/uploads/media/default/0001/01/thumb_' . $entity3->getId(). '_reference.jpg');
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10;
    }

}