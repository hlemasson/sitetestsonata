<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 22/05/13
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Sonata\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Application\Sonata\UserBundle\Entity\User;

class loadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        //user-admin
        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        $userAdmin->setEmail('admin@sonata.sandles.modemlon.net');
        $userAdmin->setPlainPassword('adminpass');
        $userAdmin->setEnabled(true);
        $userAdmin->setRoles(array('ROLE_SUPER_ADMIN'));

        //user-1
        $user1 = new User();
        $user1->setUsername('foo');
        $user1->setEmail('foo@sonata.sandles.modemlon.net');
        $user1->setPlainPassword('bar');
        $user1->setEnabled(true);
        $user1->setRoles(array('ROLE_USER'));

        //user-2
        $user2 = new User();
        $user2->setUsername('user');
        $user2->setEmail('user@sonata.sandles.modemlon.net');
        $user2->setPlainPassword('userpass');
        $user2->setEnabled(true);
        $user2->setRoles(array('ROLE_USER'));

        $userManager->updateUser($userAdmin, true);
        $userManager->updateUser($user1, true);
        $userManager->updateUser($user2, true);
        $manager->flush();

        $this->addReference('user-admin', $userAdmin);
        $this->addReference('user-1', $user1);
        $this->addReference('user-2', $user2);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }

}