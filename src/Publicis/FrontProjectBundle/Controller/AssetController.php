<?php

namespace Publicis\FrontProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use Publicis\EntityBundle\Entity\Asset;
use Publicis\EntityBundle\Entity\StaticFileAsset;

/**
 * Asset controller.
 *
 * @Route("/front/asset")
 */
class AssetController extends Controller
{
    /**
     * Lists all Asset entities.
     * @Secure(roles="ROLE_USER")
     *
	 * @Route("/", name="front_asset")
	 * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
		$dql = "SELECT a, p FROM PublicisEntityBundle:Asset a
		LEFT JOIN a.project p";
		$query = $em->createQuery($dql);
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$query,
			$this->get('request')->query->get('page', 1) /*page number*/,
			10 /*limit per page*/
		);

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * Finds and displays a Asset entity.
     * @Secure(roles="ROLE_USER")
     * 
	 * @Route("/{id}/show", name="front_asset_show")
	 * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
		$sizes = null;
		
		try
		{
			$entity = $em->getRepository('PublicisEntityBundle:Asset')->findAssetForClient($id);
			if ($entity instanceof StaticFileAsset)
				$sizes = getimagesize($entity->getAbsolutePath());
				
		}
		catch (\Doctrine\Orm\NoResultException $e)
		{
            throw $this->createNotFoundException('Unable to find Asset entity.');
		}

        return array(
            'entity'	=> $entity,
			'sizes'		=> $sizes,
		);
    }
}
