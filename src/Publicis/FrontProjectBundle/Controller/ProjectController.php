<?php

namespace Publicis\FrontProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

use Publicis\EntityBundle\Entity\Project;

/**
 * Project controller.
 *
 * @Route("/front/project")
 */
class ProjectController extends Controller
{
    /**
     * Lists all Project entities.
     * @Secure(roles="ROLE_USER")
	 *
	 * @Route("/", name="front_project")
	 * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
		$dql = "SELECT p FROM PublicisEntityBundle:Project p
		LEFT JOIN p.client c";
		$query = $em->createQuery($dql);
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$query,
			$this->get('request')->query->get('page', 1) /*page number*/,
			10 /*limit per page*/
		);

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * Finds and displays a Project entity.
     * @Secure(roles="ROLE_USER")
	 * 
	 * @Route("/{id}/show", name="front_project_show")
	 * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PublicisEntityBundle:Project')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
		
        return array(
            'entity'      => $entity,
		);
    }
}
