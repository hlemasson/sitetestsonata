<?php

namespace Publicis\FrontProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;


/**
 * default controller.
 *
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * Lists all Asset entities.
     * @Secure(roles="ROLE_USER")
     *
	 * @Route("/", name="homepage")
	 * @Template()
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl("sonata_admin_dashboard"));
    }
}
