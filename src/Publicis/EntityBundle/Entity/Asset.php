<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 11:44
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Publicis\EntityBundle\Entity\AssetRepository")
 * @ORM\Table(name="asset")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 * "asset" = "Asset",
 * "git" = "GitRepoAsset",
 * "svn" = "SvnRepoAsset",
 * "url" = "PreviewUrlAsset",
 * "file" = "StaticFileAsset"
 * })
 */
abstract class Asset
{
    static private $typeByClass = array(
        "git"	=> "Publicis\EntityBundle\Entity\GitRepoAsset",
        "url"	=> "Publicis\EntityBundle\Entity\PreviewUrlAsset",
        "file"	=> "Publicis\EntityBundle\Entity\StaticFileAsset",
        "svn"	=> "Publicis\EntityBundle\Entity\SvnRepoAsset",
    );

    static private $typeByUrl = array(
        "git"	=> "publicis_admin_git",
        "url"	=> "publicis_admin_url",
        "file"	=> "publicis_admin_file",
        "svn"	=> "publicis_admin_svn",
    );

    static public function getTypeByClass()
    {
        return Asset::$typeByClass;
    }

    static public function getTypeByUrl()
    {
        return Asset::$typeByUrl;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotBlank()
     */
    protected $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(unique=true, length=255)
     */
    protected $slug;


    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    protected $comment;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $is_active;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updated;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="assets", cascade={"detach"})
     * @ORM\JoinColumn(name="project_id", nullable=false, referencedColumnName="id")
     */
    protected $project;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", cascade={"detach"})
     * @ORM\JoinColumn(name="creator_id", nullable=false, referencedColumnName="id")
     */
    protected $creator;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", cascade={"detach"})
     * @ORM\JoinColumn(name="last_contributor_id", referencedColumnName="id")
     */
    protected $last_contributor = null;

    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Asset
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Asset
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Asset
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Asset
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive ? true : false;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Asset
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Asset
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set project
     *
     * @param \Publicis\EntityBundle\Entity\Project $project
     * @return Asset
     */
    public function setProject(\Publicis\EntityBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Publicis\EntityBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    public function __toString()
    {
        return $this->title ? $this->title : '';
    }

    /**
     * Set creator
     *
     * @param \Application\Sonata\UserBundle\Entity\User $creator
     * @return Asset
     */
    public function setCreator(\Application\Sonata\UserBundle\Entity\User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set last_contributor
     *
     * @param \Application\Sonata\UserBundle\Entity\User $lastContributor
     * @return Asset
     */
    public function setLastContributor(\Application\Sonata\UserBundle\Entity\User $lastContributor = null)
    {
        $this->last_contributor = $lastContributor;

        return $this;
    }

    /**
     * Get last_contributor
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getLastContributor()
    {
        return $this->last_contributor;
    }

    abstract public function nameCreationForm();

    abstract public function nameTemplateForm();
}