<?php

namespace Publicis\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="git_repo_asset")
 */
class GitRepoAsset extends Asset
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\Regex(
	 * 		pattern="#(^[A-Za-z0-9]+@)([A-Za-z0-9.]+)(:|\/)([A-Za-z0-9\/-]+)(\.git)$#",
	 * 		message="This value is not a valid git ssh url"
	 * )
	 */
	protected $ssh_url;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\Url(message="This value is not a valid https' url", protocols={"https"})
	 */
	protected $https_url;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $username;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $password;
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ssh_url
     *
     * @param string $sshUrl
     * @return GitRepoAsset
     */
    public function setSshUrl($sshUrl)
    {
        $this->ssh_url = $sshUrl;
    
        return $this;
    }

    /**
     * Get ssh_url
     *
     * @return string 
     */
    public function getSshUrl()
    {
        return $this->ssh_url;
    }

    /**
     * Set https_url
     *
     * @param string $httpsUrl
     * @return GitRepoAsset
     */
    public function setHttpsUrl($httpsUrl)
    {
        $this->https_url = $httpsUrl;
    
        return $this;
    }

    /**
     * Get https_url
     *
     * @return string 
     */
    public function getHttpsUrl()
    {
        return $this->https_url;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return GitRepoAsset
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return GitRepoAsset
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

	public function nameCreationForm()
	{
		return 'Publicis\EntityBundle\Form\GitRepoAssetType';
	}

	public function nameTemplateForm()
	{
		return 'Git';
	}
}