<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 11:38
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="project")
 * @ORM\HasLifecycleCallbacks()
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(unique=true, length=255)
     */
    protected $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $internal_title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $job_reference;

    /**
     * @ORM\Column(type="text")
     */
    protected $comment;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $is_active;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @ORM\OneToMany(targetEntity="Asset", mappedBy="project", cascade={"remove", "persist"})
     */
    protected $assets;
    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinColumn(name="client_id", nullable=false, referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", cascade={"detach"})
     * @ORM\JoinColumn(name="creator_id", nullable=false, referencedColumnName="id")
     */
    protected $creator;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", cascade={"detach"})
     * @ORM\JoinColumn(name="last_contributor_id", referencedColumnName="id")
     */
    protected $last_contributor = null;

    public function __construct()
    {
        $this->assets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Project
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Project
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set internal_title
     *
     * @param string $internalTitle
     * @return Project
     */
    public function setInternalTitle($internalTitle)
    {
        $this->internal_title = $internalTitle;

        return $this;
    }

    /**
     * Get internal_title
     *
     * @return string
     */
    public function getInternalTitle()
    {
        return $this->internal_title;
    }

    /**
     * Set job_reference
     *
     * @param string $jobReference
     * @return Project
     */
    public function setJobReference($jobReference)
    {
        $this->job_reference = $jobReference;

        return $this;
    }

    /**
     * Get job_reference
     *
     * @return string
     */
    public function getJobReference()
    {
        return $this->job_reference;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Project
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Project
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Project
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Project
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add assets
     *
     * @param \Publicis\EntityBundle\Entity\Asset $assets
     * @return Project
     */
    public function addAsset(\Publicis\EntityBundle\Entity\Asset $assets)
    {
        $this->assets[] = $assets;

        return $this;
    }

    /**
     * Remove assets
     *
     * @param \Publicis\EntityBundle\Entity\Asset $assets
     */
    public function removeAsset(\Publicis\EntityBundle\Entity\Asset $assets)
    {
        $this->assets->removeElement($assets);
    }

    /**
     * Get assets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * Set creator
     *
     * @param \Application\Sonata\UserBundle\Entity\User $creator
     * @return Project
     */
    public function setCreator(\Application\Sonata\UserBundle\Entity\User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set last_contributor
     *
     * @param \Application\Sonata\UserBundle\Entity\User $lastContributor
     * @return Project
     */
    public function setLastContributor(\Application\Sonata\UserBundle\Entity\User $lastContributor = null)
    {
        $this->last_contributor = $lastContributor;

        return $this;
    }

    /**
     * Get last_contributor
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getLastContributor()
    {
        return $this->last_contributor;
    }

    /**
     * Set client
     *
     * @param \Application\Sonata\UserBundle\Entity\User $client
     * @return Project
     */
    public function setClient(\Application\Sonata\UserBundle\Entity\User $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getClient()
    {
        return $this->client;
    }

    public function __toString()
    {
        return  $this->title ?  $this->title : '';
    }
}