<?php

namespace Publicis\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="svn_repo_asset")
 */
class SvnRepoAsset extends Asset
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=false)
	 * @Assert\Url(protocols={"https"})
	 * @Assert\NotBlank()
	 */
    protected $https_url;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=false)
	 * @Assert\NotBlank()
	 */
    protected $username;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=false)
	 * @Assert\NotBlank()
	 */
    protected $password;
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set https_url
     *
     * @param string $httpsUrl
     * @return SvnRepoAsset
     */
    public function setHttpsUrl($httpsUrl)
    {
        $this->https_url = $httpsUrl;
    
        return $this;
    }

    /**
     * Get https_url
     *
     * @return string 
     */
    public function getHttpsUrl()
    {
        return $this->https_url;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return SvnRepoAsset
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return SvnRepoAsset
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

	public function nameCreationForm()
	{
		return 'Publicis\EntityBundle\Form\SvnRepoAssetType';
	}

	public function nameTemplateForm()
	{
		return 'Svn';
	}
}