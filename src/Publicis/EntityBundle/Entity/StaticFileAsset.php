<?php

namespace Publicis\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\NotifyPropertyChanged;
use Doctrine\Common\PropertyChangedListener;

/**
 * @ORM\Entity
 * @ORM\Table(name="static_file_asset")
 * @ORM\HasLifecycleCallbacks
 * @ORM\ChangeTrackingPolicy("NOTIFY")
 */
class StaticFileAsset extends Asset implements NotifyPropertyChanged
{
	private $_listeners = array();
	
	public function addPropertyChangedListener(PropertyChangedListener $listener)
	{
		$this->_listeners[] = $listener;
	}
	
	protected function _onPropertyChanged($propName, $oldValue, $newValue)
	{
		if ($this->_listeners){
			foreach ($this->_listeners as $listener) {
				$listener->propertyChanged($this, $propName, $oldValue, $newValue);
			}
		}
	}
	
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
	/**
	 * @Assert\File(
	 * 		maxSize = "10M",
	 * 		mimeTypes = {"application/x-shockwave-flash", "image/gif", "image/jpeg", "image/pjpeg",
						  "image/png", "image/x-png", "image/tiff", "image/svg+xml", "image/vnd.microsoft.icon"},
			mimeTypesMessage = "Please upload a valid picture"
	 * )
	 */
    protected $file;
	
	protected $oldFilename = null;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=false)
	 */
	protected $filename;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
    protected $caption;
	
	/**
	 * @ORM\Column(type="integer", nullable=false)
	 */
    protected $size;
	
	/**
	 * @ORM\Column(type="string", length=150, nullable=false)
	 */
    protected $mime_type;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return StaticFileAsset
     */
    public function setFilename($filename)
    {
		if ($filename != $this->filename)
		{
			$this->oldFilename = $this->filename;
			$this->_onPropertyChanged('filename', $this->filename, $filename);
			$this->filename = $filename;
		}
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Get file
     *
     * @return UploadedFile 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param UploadedFile $file
     * @return StaticFileAsset
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    
		if ($this->file !== null)
		{
			if ($this->file->getExtension() !== "")
			{
				$filename = sha1(uniqid(mt_rand(), true));
				$this->setFilename($filename . '.' . $this->file->guessExtension());
			}
		}
		
        return $this;
    }

    /**
     * Set caption
     *
     * @param string $caption
     * @return StaticFileAsset
     */
    public function setCaption($caption)
    {
		if ($this->caption != $caption)
		{
			$this->_onPropertyChanged('caption', $this->caption, $caption);
			$this->caption = $caption;
		}
    
        return $this;
    }

    /**
     * Get caption
     *
     * @return string 
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return StaticFileAsset
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set mime_type
     *
     * @param string $mimeType
     * @return StaticFileAsset
     */
    public function setMimeType($mimeType)
    {
        $this->mime_type = $mimeType;
    
        return $this;
    }

    /**
     * Get mime_type
     *
     * @return string 
     */
    public function getMimeType()
    {
        return $this->mime_type;
    }
	

    /**
     * Set title
     *
     * @param string $title
     * @return StaticFileAsset
     */
    public function setTitle($title)
    {
		if ($this->title !== $title)
		{
			$this->_onPropertyChanged('title', $this->title, $title);
			$this->title = $title;
		}
    
        return $this;
    }
	

    /**
     * Set comment
     *
     * @param string $comment
     * @return StaticFileAsset
     */
    public function setComment($comment)
    {
		if ($this->comment !== $comment)
		{
			$this->_onPropertyChanged('comment', $this->comment, $comment);
			$this->comment = $comment;
		}
    
        return $this;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return StaticFileAsset
     */
    public function setIsActive($isActive)
    {
		if ($this->is_active !== $isActive)
		{
			$this->_onPropertyChanged('is_active', $this->is_active, $isActive);
			$this->is_active = $isActive;
		}
    
        return $this;
    }
	
	public function getAbsolutePath()
	{
		return null === $this->filename ? null : $this->getUploadRootDir() . '/' . $this->filename;
	}
	
	public function getWebPath()
	{
		return null === $this->filename ? null : $this->getUploadDir() . '/' . $this->filename;
	}
	
	protected function getUploadRootDir()
	{
		// the absolute directory path where uploaded files should be saved
		return __DIR__.'/../../../../web/' . $this->getUploadDir();
	}
	
	protected function getUploadDir()
	{
		return 'uploads/staticfiles';
	}
	
	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	
	public function preUpload()
	{
		if (null !== $this->file)
		{
			$this->mime_type = $this->file->getClientMimeType();
			if ($this->mime_type === null)
				$this->mime_type = "";
			$this->size = $this->file->getClientSize();
			if ($this->size === null)
				$this->size = 0;
		}
	}
	
	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload()
	{
		if (null === $this->file)
			return;
		
		
		if ($this->oldFilename != null)
		{
			$oldFilename = $this->getUploadRootDir() . '/' . $this->oldFilename;
			if (file_exists($oldFilename))
				@unlink($oldFilename);
			$this->oldFilename = null;
		}
		
		$this->file->move($this->getUploadRootDir(), $this->filename);
		unset($this->file);
	}
	
	/**
	 * @ORM\PostRemove()
	 */
	public function removeUpload()
	{
		if ($file = $this->getAbsolutePath()){
			@unlink($file);
		}
	}

	public function nameCreationForm()
	{
		return 'Publicis\EntityBundle\Form\StaticFileAssetType';
	}

	public function nameTemplateForm()
	{
		return 'File';
	}
	
}