<?php

namespace Publicis\EntityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PublicisEntityBundle extends Bundle
{

    public function getParent()
    {
        return 'SonataAdminBundle';
    }
}
