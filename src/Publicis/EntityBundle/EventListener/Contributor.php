<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 03/04/13
 * Time: 17:36
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\EventListener;


use Doctrine\ORM\Event\onFlushEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Publicis\EntityBundle\Entity\Project;
use Publicis\EntityBundle\Entity\Asset;

class Contributor
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onFlush(onFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() AS $entity)
        {
            if ($entity instanceof Project)
            {
                $entity->setLastContributor($this->container->get('security.context')->getToken()->getUser());
                $meta = $em->getClassMetadata(get_class($entity));
                $uow->recomputeSingleEntityChangeSet($meta, $entity);
            }
            else if ($entity instanceof Asset)
            {
                $entity->setLastContributor($this->container->get('security.context')->getToken()->getUser());
                $meta = $em->getClassMetadata('PublicisEntityBundle:Asset');
                $uow->recomputeSingleEntityChangeSet($meta, $entity);
            }
        }
    }
}