<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 03/04/13
 * Time: 17:35
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Publicis\EntityBundle\Entity\Project;
use Publicis\EntityBundle\Entity\Asset;

class Creator
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Project ||
            $entity instanceof Asset)
        {
            if (!$this->container->get('security.context')->getToken())
                return;
            $entity->setCreator($this->container->get('security.context')->getToken()->getUser());
        }
    }
}