<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface;

class ProjectAdmin extends Admin
{
    protected $baseRouteName = "publicis_admin_project";

    protected function configureRoutes(RouteCollection $collection)
    {
        //$collection->add('addasset', $this->getRouterIdParameter() . '/addasset');
    }

    protected  $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'title',
    );

    //protected $translationDomain = 'PublicisEntityBundle';

    public function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
              ->add('title', null, array('required' => true, 'label' => 'label.title', 'translation_domain' => 'PublicisEntityBundle'))
              ->add('comment', null, array('required' => true, 'label' => 'label.comment'))
              ->add('internal_title', null, array('required' => true, 'label' => 'label.internal_title'))
              ->add('job_reference', null, array('required' => true, 'label' => 'label.job_reference'))
              ->add('is_active', null, array('required' => false, 'label' => 'label.is_active'))
              ->add('client', null, array('required' => true, 'label' => 'label.client'))
              ->add('assets', 'sonata_type_collection', array('required' => false, 'label' => 'label.assets'), array('edit'=>'inline', 'inline'=>'table', 'sortable'=>'position'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'label.title'))
            ->add('internal_title', null, array('label' => 'label.internal_title'))
            ->add('job_reference', null, array('label' => 'label.job_reference'))
            ->add('is_active', null, array('label' => 'label.is_active'))
            ->add('client', null, array('label' => 'label.client'))
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array('label' => 'label.title'))
            ->addIdentifier('internal_title', null, array('label' => 'label.internal_title'))
            ->addIdentifier('job_reference', null, array('label' => 'label.job_reference'))
            ->addIdentifier('is_active', null, array('label' => 'label.is_active'))
            ->addIdentifier('client', null, array('label' => 'label.client'))
            ->addIdentifier('created', null, array('label' => 'label.created'))
            ->addIdentifier('updated', null, array('label' => 'label.updated'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'addasset' => array('template' => 'PublicisEntityBundle:Admin:action_addasset.html.twig'),
                )
            ))
        ;
    }

    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title', null, array('label' => 'label.title'))
            ->add('comment', null, array('label' => 'label.comment'))
            ->add('is_active', null, array('label' => 'label.is_active'))
            ->add('internal_title', null, array('label' => 'label.internal_title'))
            ->add('job_reference', null, array('label' => 'label.job_reference'))
            ->add('client', null, array('label' => 'label.client'))
            ->add('assets', null, array('label' => 'label.assets', 'template' => 'PublicisEntityBundle:Admin:show_field_assets.html.twig'))
            ->add('created', null, array('label' => 'label.created'))
            ->add('updated', null, array('label' => 'label.updated'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    //'addasset' => array('template' => 'PublicisEntityBundle:Admin:action_addasset.html.twig'),
                    'new' => array(),
                    //'list' => array(),
                )
            ))
        ;
    }

    public function getTemplate($name)
    {
        switch($name)
        {
            case 'show':
                return 'PublicisEntityBundle:Admin:project_show.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}