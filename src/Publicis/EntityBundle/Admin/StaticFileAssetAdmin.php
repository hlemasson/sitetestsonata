<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Publicis\EntityBundle\Admin\EventListener\AddValidatorFileUploadMandatory;

class StaticFileAssetAdmin extends AssetAdmin
{
    protected $baseRouteName = "publicis_admin_file";

    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        $formMapper
        ->with('File')
            ->add('caption', null, array('label' => 'label.caption'))
			->add('file', 'file', array('required' => false, 'label' => 'label.file'))
        ->end()
        ;
        $formMapper->getFormBuilder()->addEventSubscriber(new AddValidatorFileUploadMandatory());
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        parent::configureDatagridFilters($datagridMapper);

        $datagridMapper
            ->add('filename', null, array('label' => 'label.caption'))
            ->add('size', null, array('label' => 'label.caption'))
            ->add('mime_type', null, array('label' => 'label.caption'))
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);

        $listMapper
            ->addIdentifier('filename', null, array('label' => 'label.filename'))
            ->addIdentifier('size', null, array('label' => 'label.size'))
            ->addIdentifier('mime_type', null, array('label' => 'label.mime_type'))
        ;
    }

    protected function configureShowField(ShowMapper $showMapper)
    {
        parent::configureShowField($showMapper);

        $showMapper
            ->add('filename', null, array('label' => 'label.filename'))
            ->add('caption', null, array('label' => 'label.caption'))
            ->add('size', null, array('label' => 'label.size'))
            ->add('mime_type', null, array('label' => 'label.mime_type'))
        ;
    }
}