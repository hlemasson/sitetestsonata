<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 04/04/13
 * Time: 10:44
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Admin\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class NullToBoolTransformer implements DataTransformerInterface
{

    /**
     */
    public function __construct()
    {
    }

    /**
     * Transforms an int (database check) to an bool (form check).
     *
     * @param  int $dataCheck
     * @return bool
     */
    public function transform($dataCheck)
    {
        if (null === $dataCheck) {
            error_log('transform active null to 0');
            return false;
        }
        error_log('transform active to ' . $dataCheck ? '1' :  '0');
        return $dataCheck ? true : false;
    }

    /**
     * Transforms a bool (form check) to an int (database check).
     *
     * @param  bool|null $formCheck
     *
     * @return int
     */
    public function reverseTransform($formCheck)
    {
        if ($formCheck === null) {
            error_log('reverse active to 0');
            return false;
        }

        error_log('reverse active to ' . $formCheck ? 'true' :  'false');
        return $formCheck ? true : false;
    }
}