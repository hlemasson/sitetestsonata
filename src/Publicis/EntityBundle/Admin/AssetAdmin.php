<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class AssetAdmin extends Admin
{
    protected $baseRouteName = "publicis_admin_asset";
   /* protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('duplicate');
        $collection->add('view', $this->getRouterIdParameter() . '/view');
        $collection->remove('delete');
        //$collection->clearExcept(array('list', 'edit'));
    }*/

    protected  $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'created',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('General')
            ->add('title', null, array('required' => true, 'label' => 'label.title'))
            ->add('comment', null, array('required' => true, 'label' => 'label.comment'))
            ->add('is_active', null, array('required' => false, 'label' => 'label.is_active'))
            ->add('project', null, array('required' => true, 'label' => 'label.project'))
        ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'label.title'))
            ->add('is_active', null, array('label' => 'label.is_active'))
            ->add('project', null, array('label' => 'label.project'))
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array('label' => 'label.title'))
            ->addIdentifier('is_active', null, array('label' => 'label.is_active'))
            ->addIdentifier('project', null, array('label' => 'label.project'))
            ->addIdentifier('created', null, array('label' => 'label.created'))
            ->addIdentifier('updated', null, array('label' => 'label.updated'))
        ;
    }

    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title', null, array('label' => 'label.title'))
            ->add('comment', null, array('label' => 'label.comment'))
            ->add('is_active', null, array('label' => 'label.is_active'))
            ->add('project', null, array('label' => 'label.project'))
            ->add('created', null, array('label' => 'label.created'))
            ->add('updated', null, array('label' => 'label.updated'))
            //->add('url', 'string', array('template' => 'PublicisEntityBundle:AssetAdmin:show_url.html.twig'))
        ;
    }

    public function getTemplate($name)
    {
        switch($name)
        {
            case 'create':
                return 'PublicisEntityBundle:Admin:asset_create.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function getNewInstance()
    {

    }
}