<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Publicis\EntityBundle\Admin\EventListener\AddValidatorGitAsset;

class GitRepoAssetAdmin extends AssetAdmin
{
    protected $baseRouteName = "publicis_admin_git";

    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        $formMapper
        ->with('Git')
            ->add('ssh_url', null, array('required' => false, 'label' => 'label.ssh_url'))
            ->add('https_url', null, array('required' => false, 'label' => 'label.https_url'))
            ->add('username', null, array('required' => false, 'label' => 'label.username'))
            ->add('password', null, array('required' => false, 'label' => 'label.password'))
        ->end()
        ;

        $formMapper->getFormBuilder()->addEventSubscriber(new AddValidatorGitAsset());
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        parent::configureDatagridFilters($datagridMapper);

        $datagridMapper
            ->add('ssh_url', null, array('required' => true, 'label' => 'label.ssh_url'))
            ->add('https_url', null, array('required' => true, 'label' => 'label.https_url'))
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);

        $listMapper
            ->addIdentifier('ssh_url', null, array('required' => true, 'label' => 'label.ssh_url'))
            ->addIdentifier('https_url', null, array('required' => true, 'label' => 'label.https_url'))
        ;
    }

    protected function configureShowField(ShowMapper $showMapper)
    {
        parent::configureShowField($showMapper);

        $showMapper
            ->add('ssh_url', null, array('required' => true, 'label' => 'label.ssh_url'))
            ->add('https_url', null, array('required' => true, 'label' => 'label.https_url'))
            ->add('username', null, array('required' => true, 'label' => 'label.username'))
            ->add('password', null, array('required' => true, 'label' => 'label.password'))
        ;
    }

}