<?php

namespace Publicis\EntityBundle\Admin\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddValidatorGitAsset implements EventSubscriberInterface
{
	public static function getSubscribedEvents()
	{
		return array(FormEvents::POST_BIND => 'postBindValidator');
	}
	
	public function postBindValidator(FormEvent $event)
	{
		$data = $event->getData();
		$form = $event->getForm();

		if ($data->getHttpsUrl())
		{
			if (!$data->getUsername())
				$form->get('username')->addError(new FormError("This field can't be null if https is set"));
			else if	(!$data->getPassword())
				$form->get('password')->addError(new FormError("This field can't be null if https is set"));
		}
		else
		{
			if (!$data->getSshUrl())
			{
				$form->get('ssh_url')->addError(new FormError("This field or the \"Https Url\" field must be set"));
				$form->get('https_url')->addError(new FormError("This field or the \"Ssh Url\" field must be set"));
			}
			else
			{
				if ($data->getUsername())
					$form->get('username')->addError(new FormError("This field must be null if https is unset"));
				if ($data->getPassword())
					$form->get('password')->addError(new FormError("This field must be null if https is unset"));
			}
		}
	}
}