<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SvnRepoAssetAdmin extends AssetAdmin
{
    protected $baseRouteName = "publicis_admin_svn";

    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        $formMapper
        ->with('Svn')
            ->add('https_url', null, array('label' => 'label.https_url'))
            ->add('username', null, array('label' => 'label.username'))
            ->add('password', null, array('label' => 'label.password'))
        ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        parent::configureDatagridFilters($datagridMapper);

        $datagridMapper
            ->add('https_url', null, array('label' => 'label.https_url'))
            ->add('username', null, array('label' => 'label.username'))
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);

        $listMapper
            ->addIdentifier('https_url', null, array('label' => 'label.https_url'))
        ;
    }

    protected function configureShowField(ShowMapper $showMapper)
    {
        parent::configureShowField($showMapper);

        $showMapper
            ->add('https_url', null, array('label' => 'label.https_url'))
            ->add('username', null, array('label' => 'label.username'))
            ->add('password', null, array('label' => 'label.password'))
        ;
    }
}