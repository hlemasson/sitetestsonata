<?php

namespace Publicis\EntityBundle\Twig;

class PublicisExtension extends \Twig_Extension
{
	public function getFilters()
	{
		return array(
			'get_class' => new \Twig_Filter_Method($this, 'functionGetClass'),
		);
	}
	
	public function functionGetClass($entity)
	{
		return get_class($entity);
	}
	
	public function getName()
	{
		return 'publicis_extension';
	}
}