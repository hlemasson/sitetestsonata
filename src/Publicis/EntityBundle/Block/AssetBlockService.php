<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 05/04/13
 * Time: 16:55
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Block;

use Sonata\BlockBundle\Block\BaseBlockService;
use Symfony\Component\HttpFoundation\Response;

use Sonata\BlockBundle\Model\BlockInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BlockContextInterface;

class AssetBlockService extends BaseBlockService
{
    /**
     * {@inheritdoc}
     */
    function getDefaultSettings()
    {
        return array(
            'test' => 'noidea'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $block, Response $response = null)
    {
        $settings = array_merge($this->getDefaultSettings(), $block->getSettings());

        return $this->renderResponse('PublicisEntityBundle:Block:block_core_asset.html.twig', array('block' => $block->getBlock()), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Admin Asset';
    }
}