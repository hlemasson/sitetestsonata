<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 22/05/13
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
namespace Publicis\EntityBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Sonata\UserBundle\Entity\User;
use Publicis\EntityBundle\Entity\StaticFileAsset;

class loadStaticFileAssetData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /*$entity_one = new StaticFileAsset();
        $entity_one->setFilename('file1');
        $entity_one->setCaption('legend 1');
        $entity_one->setSize('487');
        $entity_one->setMimeType('png');
        $entity_one->setTitle('asset six');
        $entity_one->setComment('asset six');
        $entity_one->setIsActive(true);
        $entity_one->setProject($this->getReference('project-1'));
        $entity_one->setCreator($this->getReference('user-admin'));

        $entity_two = new StaticFileAsset();
        $entity_two->setFilename('file2');
        $entity_two->setCaption('legend 2');
        $entity_two->setSize('2045');
        $entity_two->setMimeType('pdf');
        $entity_two->setTitle('asset seven');
        $entity_two->setComment('asset seven');
        $entity_two->setIsActive(true);
        $entity_two->setProject($this->getReference('project-2'));
        $entity_two->setCreator($this->getReference('user-admin'));

        $entity_three = new StaticFileAsset();
        $entity_three->setFilename('file3');
        $entity_three->setCaption('legend 3');
        $entity_three->setSize('128');
        $entity_three->setMimeType('txt');
        $entity_three->setTitle('asset height');
        $entity_three->setComment('asset height');
        $entity_three->setIsActive(true);
        $entity_three->setProject($this->getReference('project-2'));
        $entity_three->setCreator($this->getReference('user-admin'));

        $manager->persist($entity_one);
        $manager->persist($entity_two);
        $manager->persist($entity_three);
        $manager->flush();*/

        //$this->addReference('staticfile-one', $entity_one);
        //$this->addReference('staticfile-two', $entity_two);
        //$this->addReference('staticfile-three', $entity_three);*
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 30;
    }

}