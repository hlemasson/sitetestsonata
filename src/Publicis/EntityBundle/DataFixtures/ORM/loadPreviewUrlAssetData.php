<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 22/05/13
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
namespace Publicis\EntityBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Publicis\EntityBundle\Entity\PreviewUrlAsset;

class loadPreviewUrlAssetData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity_one = new PreviewUrlAsset();
        $entity_one->setTitle('asset three');
        $entity_one->setComment('asset three');
        $entity_one->setIsActive(true);
        $entity_one->setUrl('http://google.com');
        $entity_one->setProject($this->getReference('project-1'));
        $entity_one->setCreator($this->getReference('user-admin'));

        $entity_two = new PreviewUrlAsset();
        $entity_two->setTitle('asset four');
        $entity_two->setComment('asset four');
        $entity_two->setIsActive(true);
        $entity_two->setUrl('http://translate.google.co.uk/#en/fr/');
        $entity_two->setProject($this->getReference('project-1'));
        $entity_two->setCreator($this->getReference('user-admin'));

        $entity_three = new PreviewUrlAsset();
        $entity_three->setTitle('asset five');
        $entity_three->setComment('asset five');
        $entity_three->setIsActive(true);
        $entity_three->setUrl('http://twitter.github.com/bootstrap/components.html#dropdowns');
        $entity_three->setProject($this->getReference('project-2'));
        $entity_three->setCreator($this->getReference('user-admin'));

        $manager->persist($entity_one);
        $manager->persist($entity_two);
        $manager->persist($entity_three);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 25;
    }

}