<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 22/05/13
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
namespace Publicis\EntityBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Publicis\EntityBundle\Entity\SvnRepoAsset;

class loadSvnRepoAssetData extends AbstractFixture implements OrderedFixtureInterface
{   /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity_one = new SvnRepoAsset();
        $entity_one->setTitle('asset ten');
        $entity_one->setComment('asset ten');
        $entity_one->setIsActive(true);
        $entity_one->setCreator($this->getReference('user-admin'));
        $entity_one->setProject($this->getReference('project-1'));
        $entity_one->setHttpsUrl('https://fhauif.afhuia.com');
        $entity_one->setUsername('User');
        $entity_one->setPassword('a54da8d4a86');

        $manager->persist($entity_one);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 35;
    }

}