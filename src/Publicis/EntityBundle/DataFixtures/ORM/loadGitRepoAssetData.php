<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 22/05/13
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
namespace Publicis\EntityBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Sonata\UserBundle\Entity\User;
use Publicis\EntityBundle\Entity\GitRepoAsset;

class loadGitRepoAssetData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity_one = new GitRepoAsset();
        $entity_one->setTitle('asset one');
        $entity_one->setComment('asset one');
        $entity_one->setIsActive(true);
        $entity_one->setCreator($this->getReference('user-admin'));
        $entity_one->setProject($this->getReference('project-1'));
        $entity_one->setSshUrl('git@ajiodja.adhioaa.git');
        $entity_one->setHttpsUrl('https://nhada.dajid.git');
        $entity_one->setUsername('Odfhua');
        $entity_one->setPassword('af516q');

        $entity_two = new GitRepoAsset();
        $entity_two->setTitle('asset two');
        $entity_two->setComment('asset two');
        $entity_two->setIsActive(true);
        $entity_two->setCreator($this->getReference('user-admin'));
        $entity_two->setProject($this->getReference('project-1'));
        $entity_two->setSshUrl('git@cnuainas.git');
        $entity_two->setHttpsUrl('https://ioqwenvi.git');
        $entity_two->setUsername('dhuia');
        $entity_two->setPassword('q89af51');

        $manager->persist($entity_one);
        $manager->persist($entity_two);
        $manager->flush();

        $this->addReference('git-asset-1', $entity_one);
        $this->addReference('git-asset-2', $entity_two);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 20;
    }

}