<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 22/05/13
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
namespace Publicis\EntityBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Publicis\EntityBundle\Entity\Project;

class loadProjectData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity_one = new Project();
        $entity_one->setTitle('project one');
        $entity_one->setInternalTitle('project one');
        $entity_one->setJobReference('project one');
        $entity_one->setComment('comment for project one');
        $entity_one->setIsActive(true);
        $entity_one->setCreator($this->getReference('user-admin'));
        $entity_one->setClient($this->getReference('user-2'));

        $entity_two = new Project();
        $entity_two->setTitle('project two');
        $entity_two->setInternalTitle('project two');
        $entity_two->setJobReference('project two');
        $entity_two->setComment('comment for project two');
        $entity_two->setIsActive(false);
        $entity_two->setCreator($this->getReference('user-admin'));
        $entity_two->setClient($this->getReference('user-1'));

        $manager->persist($entity_one);
        $manager->persist($entity_two);

        $manager->flush();

        $this->addReference('project-1', $entity_one);
        $this->addReference('project-2', $entity_two);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 15;
    }

}