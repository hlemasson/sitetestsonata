<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 08/04/13
 * Time: 14:16
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AssetType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', 'choice', array(
            'label' => 'label.type',
            'choices' => array(
                'git'  => 'Git',
                'url'  => 'Url',
                'svn'  => 'Svn',
                'file' => 'File',
            ),
            'multiple' => false,
            'expanded' => false,
            'required' => true,
        ));
    }

    public function getName()
    {
        return 'asset';
    }
}