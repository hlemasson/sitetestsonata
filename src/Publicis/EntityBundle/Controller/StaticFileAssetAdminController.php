<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

class StaticFileAssetAdminController extends AssetAdminController
{
    public function createAction()
    {
        return parent::createChildAction();
    }
}