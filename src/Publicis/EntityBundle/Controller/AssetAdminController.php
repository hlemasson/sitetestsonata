<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Controller;

use Publicis\EntityBundle\Entity\Asset;
use Publicis\EntityBundle\Form\Type\AssetType;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

class AssetAdminController extends Controller
{
    protected function createChildAction()
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }

        $object = $this->admin->getNewInstance();
        $project_id = $this->get('request')->query->get('project_id', null);
        if ($project_id != null)
        {
            $project = $this->getDoctrine()->getManager()->getRepository('PublicisEntityBundle:Project')->find($project_id);
            if ($project)
                $object->setProject($project);
        }
        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->getRestMethod()== 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $this->admin->create($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                $this->addFlash('sonata_flash_success','flash_create_success');
                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash('sonata_flash_error', 'flash_create_error');
                }
            }
            elseif ($this->isPreviewRequested()) {
                // pick the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'create',
            'form'   => $view,
            'object' => $object,
        ));
    }

    public function createAction()
    {
        $object = $this->admin->getNewInstance();
        $form = $this->createForm(new AssetType());
        if ($this->getRestMethod() == 'POST')
        {
            error_log('METHOD POST');
            $form->bind($this->get('request'));
            $isFormValid = $form->isValid();
            if ($isFormValid)
            {
                error_log('form is valid');
                $data = $form->getData();
                $typeByUrl = Asset::getTypeByUrl();
                $type = $data['type'];
                error_log('type = ' . $type);
                error_log(isset($typeByUrl[$type]) ? 'true' : 'false');
                if (!isset($typeByUrl[$type]))
                    $this->addFlash('sonata_flash_error', 'flash_create_error');
                else
                    return $this->redirect($this->generateUrl($typeByUrl[$type] . '_create'));
            }
        }

        return $this->render($this->admin->getTemplate('create'), array(
            'action' => 'create',
            'object' => $object,
            'form' => $form->createView(),
        ));
    }
}