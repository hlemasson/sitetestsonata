<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hlemasco
 * Date: 28/03/13
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */

namespace Publicis\EntityBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class GitRepoAssetAdminController extends AssetAdminController
{
    public function createAction()
    {
        return parent::createChildAction();
    }
}